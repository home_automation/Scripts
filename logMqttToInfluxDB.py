import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
import datetime

# Set MQTT broker configuration
BROKER_IP = "192.168.1.4"
BROKER_PORT = 1883
BROKER_KEEPALIVE = 60

# set db configuration
host = "192.168.1.4"
port = 8086
user = ""
password = ""
dbname = "test"

# Data received
temp = 0.0
press = 0.0
humid = 0.0
utcTimeISO ="1970-01-01T00:00:00Z"
topicTempStr = "home1/balc/temp"
topicPressStr = "home1/balc/press"
topicHumidStr = "home1/balc/humid"

def on_connect(client, userdata, flags, rc):
   print("Connected with result code " + str(rc))
   clientMqtt.subscribe("home1/balc/#")

def writeToSerial():
  print("Write to serial")
  print(humid)
  print(press)
  print(temp)
  print(utcTimeISO)
  print(type(utcTimeISO))

def on_message(client, userdata, msg):
   data_str = str(msg.payload.decode("utf-8"))
   if msg.topic == topicTempStr:
      global temp
      temp = float(data_str)
   if msg.topic == topicPressStr:
      global press
      press = float(data_str)
   if msg.topic == topicHumidStr:
      global humid
      humid = float(data_str)
      global utcTimeISO
      utcTimeISO = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
      writeToDB()
   # writeToDB(data[0], float(data[1]), int(data[2]), int(data[3]))

def writeToDB():
   # create json request
   json_body = [
      {
         "measurement": "clima",
         "tags": {
               "sensor": "uPLC1",
               "location": "balcony"
         },
         "time": utcTimeISO,
         "fields": {
               "temperature": temp,
               "pressure": press,
               "humidity": humid,
         }
      }
   ]
   print(json_body)
   res = clientDB.write_points(json_body, time_precision='s')
   print(res)

# create influxdb client
clientDB = InfluxDBClient(host, port, user, password, dbname)

clientMqtt = mqtt.Client()
clientMqtt.on_connect = on_connect
clientMqtt.on_message = on_message

clientMqtt.connect(BROKER_IP, BROKER_PORT, BROKER_KEEPALIVE)

clientMqtt.loop_forever()
